USE [QLSV]
GO
/****** Object:  Table [dbo].[SinhVien]    Script Date: 8/17/2022 4:37:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SinhVien](
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[ID] [nchar](10) NOT NULL,
	[Department] [nvarchar](50) NULL,
	[SexOfPerson] [nvarchar](6) NULL,
 CONSTRAINT [PK_SinhVien] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[SinhVien] ([FirstName], [LastName], [ID], [Department], [SexOfPerson]) VALUES (N'fName', N'lName', N'111       ', N'Department', N'Male')
INSERT [dbo].[SinhVien] ([FirstName], [LastName], [ID], [Department], [SexOfPerson]) VALUES (N' 4fdsf', N'sfdf', N'2312      ', N'dsfdsf', N'male')
INSERT [dbo].[SinhVien] ([FirstName], [LastName], [ID], [Department], [SexOfPerson]) VALUES (N'dasf', N'fsdf', N'3         ', N'a', N'female')
INSERT [dbo].[SinhVien] ([FirstName], [LastName], [ID], [Department], [SexOfPerson]) VALUES (N'name3', N'n3', N'333       ', N'd3', N'Male')
INSERT [dbo].[SinhVien] ([FirstName], [LastName], [ID], [Department], [SexOfPerson]) VALUES (N'n4', N'n4', N'444       ', N'd4', N'Female')
GO
