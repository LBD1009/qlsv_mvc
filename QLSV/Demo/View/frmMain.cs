﻿using Demo.View;
using Report1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace View
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void svToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserView userView = new UserView();
            userView.Show();
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRP rp = new frmRP();
            rp.ShowDialog();
        }
    }
}
