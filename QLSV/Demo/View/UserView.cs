﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;


namespace Demo.View
{
    public partial class UserView : Form
    {
        UserController _controller = new UserController();
        
        public UserView()
        {
            InitializeComponent();
        }

        bool check()
        {
            if (_controller.loadData()!=null)
            {
                return true;
            }
            return false;
        }
        private void UserView_Load(object sender, EventArgs e)
        {
            if (check())
            {
                dataGridView1.DataSource = _controller.loadData();
            }
            List<string> a = new List<string>() { "male", "female" };
       
            comboBox1.DataSource=a;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            txtFName.Text = txtLastName.Text = txtID.Text = txtDepartment.Text = String.Empty;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_controller.Delete(txtID.Text))
            {

                MessageBox.Show("Success");
                dataGridView1.DataSource = _controller.loadData();
            }
            else
                MessageBox.Show("Try again");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SinhVien sv = new SinhVien();
            sv.FirstName = txtFName.Text;
            sv.LastName = txtLastName.Text;
            sv.ID =txtID.Text; 
            sv.Department=txtDepartment.Text;
            sv.SexOfPerson = comboBox1.SelectedValue.ToString();

            if (_controller.save(sv))
            {
                MessageBox.Show("Success");
                dataGridView1.DataSource = _controller.loadData();
            }
            else
                MessageBox.Show("Try again");


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           txtFName.Text =  dataGridView1.CurrentRow.Cells[0].Value.ToString();
            txtLastName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            txtID.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            txtDepartment.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            comboBox1.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = _controller.search(txtSearch.Text);
        }
    }
}
