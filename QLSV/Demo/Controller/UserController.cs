﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Controller
{
     public class UserController
    {
        QLSVDataContext db;

        #region
        public List<SinhVien> loadData()
        {
            db = new QLSVDataContext();
            return db.SinhViens.Select(t => t).ToList(); 
        }

        public bool addNew(SinhVien sv)
        {
            try
            {
                db.SinhViens.InsertOnSubmit(sv);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool updateDGV(SinhVien sv)
        {
            try
            {
                SinhVien x = db.SinhViens.Where(q => q.ID == sv.ID).FirstOrDefault();
                x.FirstName = sv.FirstName;
                x.LastName = sv.LastName;
                x.Department = sv.Department;
                x.SexOfPerson = sv.SexOfPerson;

                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
        public bool Delete(String ID)
        {
            try
            {
                SinhVien x = db.SinhViens.Where(q => q.ID == ID).FirstOrDefault();
                db.SinhViens.DeleteOnSubmit(x);
                db.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool save(SinhVien sv)
        {
            if (db.SinhViens.Where(t => t.ID == sv.ID).Count() == 0)
            {
                addNew(sv);
                return true;
            }
            else
            {
                if (updateDGV(sv))
                {
                    return true;
                }
                return false;
            }  
        }
        #endregion

        #region //them chuc nang tim kiem
        public List<SinhVien> search(string _text)
        {
            return db.SinhViens.Where(t=>t.ID.Contains(_text)||t.FirstName.Contains(_text)||t.LastName.Contains(_text)).ToList();
        }

        #endregion
    }
}
