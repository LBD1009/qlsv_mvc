﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Report1
{
    public partial class frmRP : Form
    {
        public frmRP()
        {
            InitializeComponent();
        }

        private void frmRP_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'qLSV.SinhVien' table. You can move, or remove it, as needed.
            this.sinhVienTableAdapter.Fill(this.qLSV.SinhVien);

            this.reportViewer1.RefreshReport();
        }
    }
}
